# solicitorio

> Scans given directory for npm dependencies with unusable licenses


## Install

```
$ npm install --save solicitorio
```

## CLI

```
$ npm install --global solicitorio
```

```
$ solicitorio --help

  Usage
    solicitorio [input]

  Options
    --ignore-versions  Ignore the versions of the whitelisted dependencies. [Default: false]
    --exclude   Directories to exclude [Default: noone]
```

**Expects a .solicitoriorc in the project root**

Example .solicitoriorc: 

```
{
    "compatibleLicenses": [
        "MIT",
        "Apache",
        "Apache-2.0",
        "BSD",
        "BSDv3",
        "WTFPL",
        "CC0-1.0",
        "CC-BY-3.0"
    ],
    "warningLicenses": [
        "ISC",
        "Unlicense"
    ],
    "whiteListedModules": [
        "normalize-package-data@1.0.3",
    ]
}
```

Any licenses that are not in the compatibleLicenses or warningLicenses array will throw an error.


## License

MIT © [Marco De Jongh](http://atlassian.com)
