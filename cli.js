#!/usr/bin/env node
'use strict';
var meow = require('meow');
var solicitorio = require('./index');
var _ = require('underscore');

var cli = meow([
    'Usage',
    '  $ solicitorio [input]',
    '',
    'Options',
    '  --ignore-versions  Ignore the versions of the whitelisted dependencies. [Default: false]',
    '  --exclude          Directories to exclude [Default: noone]',
    'Examples',
    '  $ solicitorio',
    '  unicorns & rainbows',
    '  $ solicitorio ponies',
    '  ponies & rainbows'
]);

const DEFAULT_FLAGS = {
    ignoreVersions: false,
    exclude: []
};

_.defaults(cli.flags, DEFAULT_FLAGS);

solicitorio(cli.input[0], cli.flags);
